const FIRST_NAME = "Robert Cristian";
const LAST_NAME = "Gheorghe";
const GRUPA = "1083";

function initCaching() {
   var obj = {about:0,home:0,contact:0};
   obj.pageAccessCounter = function(c){
       if (c==undefined){ obj.home++; }      
       else{       
       if (c.toLowerCase()=='about'){ obj.about++; }
       if (c.toLowerCase()=='contact'){ obj.contact++; }
        }
   }
   obj.getCache = function(){ return obj; }
   return obj;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}
